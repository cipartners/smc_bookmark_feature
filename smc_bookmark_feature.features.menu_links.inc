<?php
/**
 * @file
 * smc_bookmark_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function smc_bookmark_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_bookmarks:bookmarks
  $menu_links['main-menu_bookmarks:bookmarks'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'bookmarks',
    'router_path' => 'bookmarks',
    'link_title' => 'Bookmarks',
    'options' => array(
      'identifier' => 'main-menu_bookmarks:bookmarks',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Bookmarks');


  return $menu_links;
}
