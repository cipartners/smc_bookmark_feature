<?php
/**
 * @file
 * smc_bookmark_feature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function smc_bookmark_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function smc_bookmark_feature_node_info() {
  $items = array(
    'bookmark' => array(
      'name' => t('Boookmark'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
